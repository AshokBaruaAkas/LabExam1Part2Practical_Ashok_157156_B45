<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chess board</title>
    <link rel="stylesheet" href="style.css">
</head>
<h2>Chess Board Creator</h2>
<form method="post">
    <input type="number" name="number" min="2" max="15" required>
    <br>
    <input type="submit" value="Create Now" name="submit">
</form>
<div class="chessBoard">
<?php
    if(isset($_POST['number'])){
        $value = $_POST['number'];
        for($i = 1;$i <= $value;$i++){
            for($j = 1;$j <= $value;$j++){
                echo "<div class='box' ";
                if ($i % 2 !== 0) {
                    if ($j % 2 !== 0) {
                        echo "style='background: white'";
                    } else {
                        echo "style='background: black'";
                    }
                } else {
                    if ($j % 2 !== 0) {
                        echo "style='background: black'";
                    } else {
                        echo "style='background: white'";
                    }
                }
                echo "></div>";
            }
            echo "<br>";
        }
    }
?>
</div>
</body>
</html>