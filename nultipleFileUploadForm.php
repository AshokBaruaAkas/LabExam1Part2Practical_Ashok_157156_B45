<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Choice Multiple File.</title>
    <style>
        *{
            text-align: center;
        }
        h2 {
            color: #19B2F9;
            font-size: 30px;
        }
        input{
            margin: 10px;
        }
    </style>
</head>
<body>
<h2>Multiple File Uploading</h2>
<form action="processMultipleFile.php" method="post" enctype="multipart/form-data">
    <input type="file" name="fileUpload[]" multiple="multiple">
    <br>
    <input type="submit" value="Upload File">
</form>
</body>
</html>